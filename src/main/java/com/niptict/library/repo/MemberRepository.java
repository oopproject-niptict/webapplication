package com.niptict.library.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.niptict.library.model.Member;

public interface MemberRepository extends CrudRepository<Member,Integer>{

	List<Member> findByStuIdIgnoreCase(String q);

}
