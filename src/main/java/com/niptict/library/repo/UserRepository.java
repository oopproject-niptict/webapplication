package com.niptict.library.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.niptict.library.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	@Query("SELECT u from User u where role='USER'")
	List<User> getMembersList();

	@Query("select u from User u where u.member.stuId like :q")
	List<User> findByMemberStuIdIgnoreCase(@Param("q") String q);

	List<User> findByLastNameIgnoreCase(String q);

	List<User> findByFirstNameIgnoreCase(String q);

	User findByMemberId(int id);

	User findByEmail(String email);

	List<User> findByMemberStuIdIgnoreCaseContaining(String q);

	List<User> findByLastNameIgnoreCaseContaining(String q);

	List<User> findByFirstNameIgnoreCaseContaining(String q);

	List<User> findByEmailIgnoreCaseContaining(String email, Pageable pageable);

}
