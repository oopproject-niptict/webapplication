package com.niptict.library.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.niptict.library.model.Book;
import com.niptict.library.model.Category;

public interface BookRepository extends JpaRepository<Book, Integer>{
	
	@Query("select b from Book b")
	List<Book> getBooks(Pageable pageable);

	List<Book> findByTitleIgnoreCase(String keyword, Pageable pageable);

	List<Book> findByCategoryNameAndTitleIgnoreCaseContaining(String category, String keyword, Pageable pageable);

	Long countByCategoryNameAndTitleIgnoreCase(String category, String keyword);

	Book findById(Long id);
	
	List<Book> findAllByCategoryId(Long id,Pageable pageable);

	Long countByCategoryId(Long catId);

	Long countByTitleIgnoreCase(String keyword);

	List<Book> findByTitleIgnoreCaseContaining(String keyword, Pageable pageable);

	List<Book> findByIsbnIgnoreCaseContaining(String q, Pageable pageable);

	Long countByIsbnIgnoreCase(String q);

	List<Book> findByCategoryNameAndIsbnIgnoreCase(String cat, String q, Pageable pageable);

	Long countByCategoryNameAndIsbnIgnoreCase(String cat, String q);

	List<Book> findByCategoryIdAndTitleIgnoreCase(Long cat, String q, Pageable pageable);

	Long countByCategoryIdAndTitleIgnoreCase(Long cat, String q);

	List<Book> findByCategoryIdAndIsbnIgnoreCase(Long cat, String q, Pageable pageable);

	Long countByCategoryIdAndIsbnIgnoreCase(Long cat, String q);

	List<Book> findAllByCategoryName(String cat, Pageable pageable);

	Long countByCategoryName(String cat);

	List<Book> findByCategoryId(Long cat);

	Book findByIsbn(String isbn);

	List<Book> findByCategoryId(Long catId, Pageable pageable);

	int countByAuthor(String name);
	
	List<Book> findByAuthor(String name, Pageable pageable);

	List<Book> findByTitleIgnoreCaseContaining(String query);

	Long countByTitleIgnoreCaseContaining(String keyword);

	Long countByCategoryNameAndTitleIgnoreCaseContaining(String category, String keyword);

	List<Book> findByCategoryNameAndIsbnIgnoreCaseContaining(String category, String query, Pageable pageable);

	Long countByIsbnIgnoreCaseContaining(String q);

	Long countByCategoryNameAndIsbnIgnoreCaseContaining(String cat, String q);




}
