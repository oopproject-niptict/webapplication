package com.niptict.library.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.niptict.library.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Integer>{
	@Query("select c from Category c")
	List<Category> getAll();

	@Query("select id from Category c where name= :categoryname")
	Long findIdByName(@Param("categoryname") String categoryname);

	Category findById(Long id);

	Category findOneByName(String category);


}
