package com.niptict.library.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.niptict.library.model.BorrowBook;

public interface BorrowBookRepository extends JpaRepository<BorrowBook, Integer>{

	int countByBookId(Long id);

	List<BorrowBook> findByBookId(Long id);

	List<BorrowBook> findByReturned(boolean b);

	BorrowBook findOneById(Long id);

	List<BorrowBook> findByUserEmailAndReturned(String email, boolean b);

	int countByBookIdAndReturned(Long id, boolean b);

	List<BorrowBook> findByBookIdAndReturned(Long id, boolean b);

}
