package com.niptict.library.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.niptict.library.model.Book;
import com.niptict.library.model.BorrowBook;
import com.niptict.library.model.Category;
import com.niptict.library.model.Member;
import com.niptict.library.model.Role;
import com.niptict.library.model.User;
import com.niptict.library.repo.BookRepository;
import com.niptict.library.repo.BorrowBookRepository;
import com.niptict.library.repo.CategoryRepository;
import com.niptict.library.repo.MemberRepository;
import com.niptict.library.repo.UserRepository;
import com.niptict.library.storage.StorageService;



@Controller
@RequestMapping("/admin")
public class AdminController {
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private BorrowBookRepository borrowBookRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private MemberRepository memberRepository;
	
	private final StorageService storageService;
	
	@Autowired
    public AdminController(StorageService storageService) {
        this.storageService = storageService;
    }
	
	static int ITEM_COUNT = 15;
	
	@RequestMapping("")
	public String dashboard(@RequestParam(defaultValue="1") int page,Model model){
		Pageable pageable = new PageRequest(page-1, ITEM_COUNT , Sort.Direction.DESC, "id");
		List<Book> books = bookRepository.getBooks(pageable);
		for(Book book:books){
			int borrowed = borrowBookRepository.countByBookIdAndReturned(book.getId(),false);
			int all = book.getAmount();
			book.setAvailable(all-borrowed);
		}
		model.addAttribute("books",books);
		model.addAttribute("categories", categoryRepository.getAll());
		model.addAttribute("currentPage", page);
		model.addAttribute("total", bookRepository.count()/ITEM_COUNT);
		return "admin/dashboard";
	}
	
	@RequestMapping("/book/add")
	public String addBook(Book book, Model model){
		model.addAttribute("categories",categoryRepository.findAll());
		return "admin/addBook";
	}
	
	@RequestMapping(value="/book/save", method=RequestMethod.POST)
	public String saveBook(@ModelAttribute Book book, @RequestParam Long categoryId, @RequestParam MultipartFile thumbnail){
		if(book.getThumbnailUrl()==null){
			writeImage(thumbnail,book);
		}else if(!(book.getThumbnailUrl()==null) && thumbnail.isEmpty()){
			//do nothing
		}else{
			book.setThumbnailUrl("");
			writeImage(thumbnail,book);
		}
		book.setCategory(null);
		Category cat = categoryRepository.findById(categoryId);
		book.setCategory(cat);
		if(book.getId()==null){
			bookRepository.save(book);
		}else{
			bookRepository.saveAndFlush(book);
		}
		return "redirect:/admin?save=success";
	}
	private boolean writeImage(MultipartFile file, Book book){
		try {
		String imageName = storageService.store(file);
				book.setThumbnailUrl("/images/"+imageName);
		} catch (Exception e) {
			e.printStackTrace();
		}return true;
	}
	@RequestMapping("/category/view")
	public String category(Model model, Category category){
		model.addAttribute("categories", categoryRepository.findAll());
		return "admin/category";
	}
	@RequestMapping("/category/save")
	public String saveCate(@ModelAttribute Category category){
		Long id = categoryRepository.findIdByName(category.getName());
		if(id==null){
			categoryRepository.save(category);
			return "redirect:/admin/category/view?add=success";
		}
		else{
			
			return "redirect:/admin/category/view?add=fail";
		}
		
	}
	
	@RequestMapping(value="/category/delete", method=RequestMethod.POST)
	public String deleteCate(@RequestParam Long id){
		Category cat = categoryRepository.findById(id);
		categoryRepository.delete(cat);
		return "redirect:/admin/category/view?delete=success";
	}
	@RequestMapping("/student/view")
	public String viewStudent(Model model){
		model.addAttribute("members", userRepository.getMembersList());
		return "admin/viewStudent";
	}
	
	@RequestMapping("/student/new")
	public String addNewStudent(User user, Member member,Model model){
		return "admin/addNewStudent";
	}
	@RequestMapping("/student/save")
	public String saveStudent(@ModelAttribute User user, @ModelAttribute Member member){
		memberRepository.save(member);
		user.setMember(member);
		user.setRole(Role.USER);
		user.setPassword("password");
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		System.out.println(user.getGender());
		userRepository.saveAndFlush(user);
		return "redirect:/admin/student/view?save=success";
	}
	@RequestMapping("/student/delete")
	public String deleteS(@RequestParam Long id){
		userRepository.delete(id);
		return "redirect:/admin/student/view?delete=success";
	}
	@RequestMapping("/student/edit")
	public String editS(@RequestParam Long id,Model model){
		User u = userRepository.findOne(id);
		model.addAttribute("user", u);
		model.addAttribute("member", u.getMember());
		return "admin/addNewStudent";
	}
	@RequestMapping("/book/return")
	public String returnBook(Model model){
		List<BorrowBook> borrows=borrowBookRepository.findByReturned(false);
		for(BorrowBook borrow : borrows){
			Calendar c = Calendar.getInstance();
			c.setTime(borrow.getBorrowDate());
			c.add(Calendar.DATE, borrow.getDeadline());
			Date deadline = c.getTime();
			borrow.setDueDate(deadline);
		}
		model.addAttribute("borrows",borrows);
		return "admin/returnBook";
	}
	@RequestMapping("/book/checkout")
	public String checkOutBook(Model model){
		return "admin/checkOutBook";
	}
	@RequestMapping("/book/edit")
	public String editBook(@RequestParam Long id, Model model){
		model.addAttribute("book",bookRepository.findById(id));
		model.addAttribute("categories",categoryRepository.findAll());
		return "admin/addBook";
	}
	@RequestMapping("/book/delete")
	public String deleteBook(@RequestParam Long id){
		Book b = bookRepository.findById(id);
		if(b !=null){
			List<BorrowBook> borrows = borrowBookRepository.findByBookId(b.getId());
			borrowBookRepository.delete(borrows);
			bookRepository.delete(b);
		}

		return "redirect:/admin?delete=success";
	}
	private String testCatagory[]={
			"Novel",
			"Science-Fiction",
			"Self-improvement",
			"Programming Language"
		};
		private String testTitle[]={
			"The Road",
			"Intepreter of Maladie",
			"Middlesex: The novel",
			"Gilead: A novel",
			"Kavalier & Klay",
			"A canticle for leibowitz",
			"The Corrections"
		};
		private String testAuthor[]={
			"Cormac McCarthy",
			"Jhumpa Lahiri",
			"Jeffrey Eugenides",
			"Marilynne Robinson",
			"michael chabon",
			"walter m. miller jr.",
			"jonathan franzen"
		};
		private String testPublishYear[]={
			"1997",
			"1998",
			"1999",
			"2000",
			"2001",
			"2002",
			"2003"
		};
		private String testUrl[]={
			"/image/page1_pic1.jpg",
			"/image/page1_pic2.jpg",
			"/image/page1_pic3.jpg",
			"/image/page1_pic4.jpg",
			"/image/page1_pic5.jpg",
			"/image/page1_pic6.jpg",
			"/image/page1_pic7.jpg",
		};
		private int testAmount[]={
			1,2,3,4,5,6,7
		};
		private String testIsbn[]={
			"ISBN-1111-1111-1111",
			"ISBN-2222-2222-2222",
			"ISBN-3333-3333-3333",
			"ISBN-4444-4444-4444",
			"ISBN-5555-5555-5555",
			"ISBN-6666-6666-6666",
			"ISBN-7777-7777-7777"
		};
		private int edition=1;
		private String description = "Testing book";
		@RequestMapping("/init/test")
		public @ResponseBody String test(){
			List<Category> cat = categoryRepository.getAll();
			for(int i=0;i<100;i++){
				Book b = new Book();
				Random d= new Random();
				int o = d.nextInt(7);
				b.setAmount(testAmount[o]);
				b.setAuthor(testAuthor[o]);
				b.setDescription(description);
				Random r = new Random();
				int a= r.nextInt(1000);
				b.setIsbn(testIsbn[o]+a);
				b.setPublishYear(testPublishYear[o]);
				b.setThumbnailUrl(testUrl[o]);
				b.setTitle(testTitle[o]+a);
				Random rand = new Random();
				int  n = rand.nextInt(4);
				b.setCategory(cat.get(n));
				b.setEdition(edition);
				bookRepository.save(b);
			}
			return "ok";
		}
		@RequestMapping("/init/testCategory")
		public @ResponseBody String testCate(){
			for(int i=0;i<testCatagory.length;i++){
				Category category= new Category();
				category.setName(testCatagory[i]);
				categoryRepository.save(category);
			}
			return "ok";
		}
		@RequestMapping("/search")
	    public String search(@RequestParam(defaultValue="") String category,@RequestParam(defaultValue="") String query,@RequestParam(defaultValue="1") int page,@RequestParam(defaultValue="title") String type, Model model) {
			Pageable pageable = new PageRequest(page-1,15, Sort.Direction.DESC, "id");
			List<Book> books;
			Long bookCount = null;
			if(query.isEmpty()&&!category.isEmpty()){
				books=bookRepository.findAllByCategoryName(category,pageable);
				bookCount=bookRepository.countByCategoryName(category);
			}
			else if(type.equals("title")){
				if(category.isEmpty()){
					books = bookRepository.findByTitleIgnoreCaseContaining(query,pageable);
					bookCount=bookRepository.countByTitleIgnoreCaseContaining(query);
				}else{
					books = bookRepository.findByCategoryNameAndTitleIgnoreCaseContaining(category, query, pageable);
					bookCount=bookRepository.countByCategoryNameAndTitleIgnoreCaseContaining(category, query);
				}
			}
			else{
				if(category.isEmpty()){
					books = bookRepository.findByIsbnIgnoreCaseContaining(query,pageable);
					bookCount=bookRepository.countByIsbnIgnoreCaseContaining(query);
				}else{
					books = bookRepository.findByCategoryNameAndIsbnIgnoreCase(category, query, pageable);
					bookCount=bookRepository.countByCategoryNameAndIsbnIgnoreCaseContaining(category, query);
				}
			}
			if(books.size()==0){
				model.addAttribute("no","");
			}
			for(Book book:books){
				int borrowed = borrowBookRepository.countByBookIdAndReturned(book.getId(),false);
				int all = book.getAmount();
				book.setAvailable(all-borrowed);
			}
			model.addAttribute("parameter",category);
			model.addAttribute("categories",categoryRepository.findAll());
			model.addAttribute("books", books);
			model.addAttribute("currentPage", page);
			model.addAttribute("total",bookCount/15);
			return "/admin/dashboard";
	    }
		@RequestMapping("/auto-complete")
		@ResponseBody
	    public String autosearch(@RequestParam(defaultValue="") String category,@RequestParam(defaultValue="") String query,@RequestParam String type, Model model) {
			System.out.println(type);
			Pageable pageable = new PageRequest(0,5, Sort.Direction.DESC, "id");
			List<Book> books;
			if(type.equals("title")){
				if(category.isEmpty()){
					books = bookRepository.findByTitleIgnoreCaseContaining(query,pageable);
				}else{
					books = bookRepository.findByCategoryNameAndTitleIgnoreCaseContaining(category, query, pageable);
				}
			}
			else{
				if(category.isEmpty()){
					books = bookRepository.findByIsbnIgnoreCaseContaining(query,pageable);
					System.out.println(query);
				}else{
					books = bookRepository.findByCategoryNameAndIsbnIgnoreCaseContaining(category, query, pageable);
				}
			}
			return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(books);
	    }
		//TODO
		@RequestMapping("/student/search")
		public String searchs(@RequestParam String q, @RequestParam String by,Model model){
			List<User> students;
			if(by.equals("id")){
				students=userRepository.findByMemberStuIdIgnoreCaseContaining(q);
			}else if(by.equals("last")){
				students=userRepository.findByLastNameIgnoreCaseContaining(q);
			}else{
				students=userRepository.findByFirstNameIgnoreCaseContaining(q);
			}
			model.addAttribute("members",students);
			return "/admin/viewStudent";
		}
		@RequestMapping("/auto-isbn")
		@ResponseBody
		public String autoBook(@RequestParam String isbn){
			Pageable pageable = new PageRequest(0,5, Sort.Direction.DESC, "id");
			List<Book> books = bookRepository.findByIsbnIgnoreCaseContaining(isbn, pageable);
			String js =new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(books);
			System.out.println(js);
			return js;
		}
		@RequestMapping("/auto-member")
		@ResponseBody
		public String automember(@RequestParam String email){
			Pageable pageable = new PageRequest(0,5, Sort.Direction.DESC, "id");
			List<User> users = userRepository.findByEmailIgnoreCaseContaining(email,pageable);
			String json=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(users);
			System.out.println(json);
			return json;
		}
		@RequestMapping(value="/checkout/confirm" ,method=RequestMethod.POST)
		public String confirmCheckout(@RequestParam String isbn,@RequestParam String email, @RequestParam int day){
			Book book = bookRepository.findByIsbn(isbn);
			if(book==null){
				return "redirect:/admin/book/checkout?error=b";
			}
			else{
				User user = userRepository.findByEmail(email);
				if(!(user==null)){
					List<BorrowBook> b = borrowBookRepository.findByUserEmailAndReturned(email,false);
					System.out.println(b.toString());
					if(b==null || b.isEmpty()){
						BorrowBook borrow = new BorrowBook();
						borrow.setBook(book);
						borrow.setUser(user);
						borrow.setDeadline(day);
						borrow.setReturned(1==2);
						borrow.setBorrowDate(new Date());
						borrowBookRepository.save(borrow);
						return "redirect:/admin/book/return?borrow=success";
					}else{
						return "redirect:/admin/book/return?borrow=fail";
					}
				}
				else{
					return "redirect:/admin/book/checkout?error=u";
				}
			}
		}
		@RequestMapping(value="/book/return/confirm")
		public String confirmReturn(@RequestParam Long id, Model model){
			BorrowBook borrow = borrowBookRepository.findOneById(id);
			//get deadline
			Calendar c = Calendar.getInstance();
			c.setTime(borrow.getBorrowDate());
			c.add(Calendar.DATE, borrow.getDeadline());
			Date deadline = c.getTime();
			borrow.setDueDate(deadline);
			Date today = new Date();
			//compare to current date
			if(borrow.getDueDate().after(today) || borrow.getDueDate().equals(today) ){
				borrow.setReturned(1==1);
				borrowBookRepository.save(borrow);
				return "redirect:/admin/book/return";
			}else{
				Long time = today.getTime()-borrow.getDueDate().getTime();
				long day = TimeUnit.DAYS.convert(time, TimeUnit.MILLISECONDS);
				long fine=day*2000;
				model.addAttribute("fine", fine);
				model.addAttribute("day",day);
				model.addAttribute("id",id);
				List<BorrowBook> borrows=borrowBookRepository.findByReturned(false);
				for(BorrowBook borrower : borrows){
					Calendar ca = Calendar.getInstance();
					ca.setTime(borrow.getBorrowDate());
					ca.add(Calendar.DATE, borrower.getDeadline());
					Date deadlinee = c.getTime();
					borrow.setDueDate(deadlinee);
				}
				model.addAttribute("borrows",borrows);
				return "/admin/returnBook";
			}
		}
		@RequestMapping(value="/return/late/confirm")
		public String paid(Long id){
			BorrowBook borrow = borrowBookRepository.findOneById(id);
			borrow.setReturned(1==1);
			borrowBookRepository.save(borrow);
			return "redirect:/admin/book/return?paid=success";
		}
}
