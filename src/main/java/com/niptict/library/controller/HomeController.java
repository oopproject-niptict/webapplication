package com.niptict.library.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.niptict.library.model.Book;
import com.niptict.library.model.BorrowBook;
import com.niptict.library.model.Category;
import com.niptict.library.model.Role;
import com.niptict.library.model.User;
import com.niptict.library.repo.BookRepository;
import com.niptict.library.repo.BorrowBookRepository;
import com.niptict.library.repo.CategoryRepository;
import com.niptict.library.repo.UserRepository;
import com.niptict.library.storage.StorageService;



@Controller
public class HomeController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private BorrowBookRepository borrowBookRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
//	@Autowired
//	private MemberRepository memberRepository;
	
	private final StorageService storageService;
	
	@Autowired
    public HomeController(StorageService storageService) {
        this.storageService = storageService;
    }
	
	static int ITEM_COUNT = 9;
	
	@RequestMapping("/")
	public String index(Model model){
		Pageable pageable = new PageRequest(0,ITEM_COUNT, Sort.Direction.DESC, "id");
		//get all category
		List<Category> cats= categoryRepository.findAll();
		//select 9 book only for each category
		Iterator<Category> i = cats.iterator();
		while (i.hasNext()){
			Category c = i.next();
			if(c.getBooks().isEmpty()){
				i.remove();
			}else if(c.getBooks().size()>9){
				for (int j=0;j<c.getBooks().size();j++){
					if(j>8){
						c.getBooks().remove(j);
						j--;
					}
				}
			}
		}
		//send book to view
		model.addAttribute("categories",cats);
		model.addAttribute("books",bookRepository.getBooks(pageable));
		return "newindex";
	}
	@RequestMapping("/more")
	public String more(@RequestParam(defaultValue="1") int page,@RequestParam(defaultValue="") String category, Model model){
		Pageable pageable = new PageRequest(page-1,ITEM_COUNT, Sort.Direction.DESC,"id");
		Long catId = categoryRepository.findIdByName(category);
		List<Book> books = null;
		Long countBook = null;
		if(category.equals("")){
			books=bookRepository.getBooks(pageable);
			countBook=bookRepository.count();
		}else if(!category.isEmpty()){
			books=bookRepository.findAllByCategoryId(catId, pageable);
			countBook=bookRepository.countByCategoryId(catId);
		}
		List<Category> cats= categoryRepository.findAll();
		Iterator<Category> i = cats.iterator();
		while (i.hasNext()){
			Category c = i.next();
			if(c.getBooks().isEmpty()){
				i.remove();
			}
		}
		model.addAttribute("parameter",category);
		model.addAttribute("categories",cats);
		model.addAttribute("books",books);
		model.addAttribute("currentPage", page);
		model.addAttribute("total",countBook/9);
		return "more";
	}
	@RequestMapping("/search")
    public String search(@RequestParam(defaultValue="") String category,@RequestParam String keyword,@RequestParam(defaultValue="1") int page, Model model) {
		Pageable pageable = new PageRequest(page-1,6, Sort.Direction.DESC, "id");
		List<Book> books;
		Long bookCount = null;
		if(category.isEmpty()){
			books = bookRepository.findByTitleIgnoreCaseContaining(keyword,pageable);
			bookCount=bookRepository.countByTitleIgnoreCaseContaining(keyword);
		}else{
			books = bookRepository.findByCategoryNameAndTitleIgnoreCaseContaining(category, keyword, pageable);
			bookCount=bookRepository.countByCategoryNameAndTitleIgnoreCaseContaining(category, keyword);
			System.out.println(books.size());
		}
		if(books.size()==0){
			model.addAttribute("no","");
		}
		System.out.println(bookCount);
		model.addAttribute("parameter",category);
		model.addAttribute("keyword",keyword);
		model.addAttribute("categories",categoryRepository.findAll());
		model.addAttribute("books", books);
		model.addAttribute("currentPage", page);
		model.addAttribute("total",bookCount/6);
		return "search-result";
    }
	
	@RequestMapping(value="/auto-complete", method=RequestMethod.GET)
	@ResponseBody
	public String forAjax(@RequestParam String query, @RequestParam(defaultValue="") String category){
		Pageable pageable = new PageRequest(0,5, Sort.Direction.DESC, "id");
		List<Book> books;
		if(category.isEmpty()){
			books=bookRepository.findByTitleIgnoreCaseContaining(query,pageable);
		}
		else{
			books=bookRepository.findByCategoryNameAndTitleIgnoreCaseContaining(category, query, pageable);
		}
		String g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(books);
		System.out.println(g);
		return g;
	}
	@GetMapping("/images/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename()+"\"")
                .body(file);
    }
	
	@RequestMapping("/test")
	@ResponseBody
	public String initAdmin(){
		User a = new User();
		a.setEmail("admin@gmail.com");
		a.setPassword("admin");
		a.setRole(Role.ADMIN);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		a.setPassword(passwordEncoder.encode(a.getPassword()));
		userRepository.save(a);
		System.out.println(a.getRole());
		return "ok";
	}
	
	@RequestMapping("/detail")
	public String book(@RequestParam Long id, Model model){
		Pageable pageable = new PageRequest(0,9, Sort.Direction.DESC, "id");
		Book book = bookRepository.findById(id);
		int borrowed = borrowBookRepository.countByBookIdAndReturned(id,false);
		int all = book.getAmount();
		book.setAvailable(all-borrowed);
		Category cat = book.getCategory();
		Long catId = cat.getId();
		List<Book> fbooks=bookRepository.findByCategoryId(catId, pageable);
		for(Book fbook : fbooks){
			if(fbook.getId().equals(id)){
				fbooks.remove(fbook);
				break;
			}
		}
		if(!fbooks.isEmpty()){
			model.addAttribute("fbooks",fbooks);
		}
		
		List<BorrowBook> borrowers = borrowBookRepository.findByBookIdAndReturned(id,false);
		if(!borrowers.isEmpty()){
			for(BorrowBook borrow : borrowers){
				Calendar c = Calendar.getInstance();
				c.setTime(borrow.getBorrowDate());
				c.add(Calendar.DATE, borrow.getDeadline());
				Date deadline = c.getTime();
				borrow.setDueDate(deadline);
			}
			model.addAttribute("borrowers",borrowers);	
		}
		model.addAttribute("dbook", book);
		List<Category> cats= categoryRepository.findAll();
		model.addAttribute("categories",cats);
		return "book";
	}
	@RequestMapping("/author")
	public String author(@RequestParam String name, Model model,@RequestParam(defaultValue="1") int page){
		Pageable pageable = new PageRequest(0,8, Sort.Direction.DESC, "id");
		List<Book> books = bookRepository.findByAuthor(name,pageable);
		model.addAttribute("books", books);
		model.addAttribute("currentPage", page);
		model.addAttribute("total",bookRepository.countByAuthor(name)/8);
		return "search-result";
	}
}
